class Connecter extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            name: '',
            etat: 'invite'
        }
    }

    render(){
        return(
            <>
                <h2>Salut {this.state.name || 'invite'} !</h2>
                {
                    this.state.etat && this.state.etat === 'invite'
                    ? <p>Connectez vous pour bénéficier des nouvelles fonctionnalités.</p> 
                    : ''
                }
                <BoutonInscription customText={this.state.etat && this.state.etat !== 'invite' ? 'S\'inscrire' : 'Se connecter'} />
            </>
        )
    }


}