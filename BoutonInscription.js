class BoutonInscription extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            connected: false
        }
    }

    render(){

        if(this.state.connected){
            return (
                <p className="connecting">Connecting...</p>
            )
        }

        return(
            <button onClick={()=> this.setState({connected: true})}>
                {this.props.customText}
            </button>
        )
    }


}